# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="a library written in C that connects with several Nintendo Wii remotes. Supports motion sensing, IR tracking, nunchuk, classic controller, and the Guitar Hero 3 controller. Single threaded and nonblocking makes a light weight and clean API."
HOMEPAGE="http://www.wiiuse.net/"
SRC_URI="http://pupinux.altervista.org/wiiuse_v0.12_src.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

IUSE="examples"
DEPEND="net-wireless/bluez-libs
	examples? ( media-libs/libsdl )"

src_unpack() {

    unpack ${A}
}

src_compile() {

    cd wiiuse_v0.12
    if use examples ; then
	emake || die "emake all failed"
    else
	emake wiiuse || die "emake wiiuse failed"
    fi
}

src_install() {

    cd wiiuse_v0.12
    emake DESTDIR="${D}" install || die "install failed"
}