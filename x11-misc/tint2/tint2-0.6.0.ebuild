# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Lightweight gtk based taskbar, freedesktop compliant"
HOMEPAGE="http://code.google.com/p/tint2/"
SRC_URI="http://tint2.googlecode.com/files/tint-0.6.0.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

IUSE=""
DEPEND="x11-libs/cairo
	x11-libs/pango
	media-libs/imlib2
	x11-libs/libXinerama"

src_unpack() {
    unpack ${A}
}

src_compile() {
    cd ${WORKDIR}/tint/src
    emake || die "emake failed"
}

src_install() {
    cd ${WORKDIR}/tint/src
    emake DESTDIR="${D}" install || die "install failed"
}